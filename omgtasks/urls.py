from django.contrib import admin
from django.urls import path, include
from django.contrib.auth import views as auth_views
from tasks import views

urlpatterns = [
    path('', views.index),
    path('task/<int:id>/', views.task_view, name="task"),
    path('task/<int:id>/edit/', views.edit_task, name="edit_task"),
    path('tasks/<int:id>/delete/', views.delete_task, name="delete_task"),
    path('task/new/', views.new_task, name="new_task"),
    
    path('login/', auth_views.LoginView.as_view(), name="login"),
    path('logout/', auth_views.LogoutView.as_view()),

    path('register/', views.register, name="register"),
    path('profile/', views.profile, name="profile"),
    path('profile_change/', views.profile_change, name="profile_change"),
    # path('password_forgot/', views.password_forgot, name="password_forgot"),
    path('password_change/', views.password_change, name="password_change"),

    path('password-reset/', auth_views.PasswordResetView.as_view(template_name='registration/password_reset_form1.html'), name='password_reset'),
    path('password-reset/done/', auth_views.PasswordResetDoneView.as_view(template_name='registration/password_reset_done1.html'), name='password_reset_done'),
    path('password-reset/confirm/<uidb64>/<token>', auth_views.PasswordResetConfirmView.as_view(template_name='registration/password_reset_confirm1.html'), name='password_reset_confirm'),
    path('password-reset/complete/', auth_views.PasswordResetCompleteView.as_view(template_name='registration/password_reset_complete1.html'), name='password_reset_complete'),

    path('admin/', admin.site.urls),
    path('select2/', include('django_select2.urls')),
]



