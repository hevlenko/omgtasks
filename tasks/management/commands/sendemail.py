from django.core.management.base import BaseCommand
from django.core.mail import send_mail
from django.contrib.auth.models import User

from datetime import date

from tasks.models import Task

class Command(BaseCommand):

    def handle(self, *args, **options):
        users = User.objects.all()
        today = date.today()
        tasks_for_today = Task.objects.filter(added__year=today.year, added__month=today.month, added__day=today.day)
        
        subject = 'OMGTasks newsletter'
        message = '<h1>Today\'s tasks: </h1>'
        from_adress = 'omgtask.space'
        recipient_list = []

        for task in tasks_for_today:
            message += '<p>' + task.title + '</p>'


        for user in users:
            recipient_list.append(user.email)

        if (tasks_for_today and recipient_list):
            send_mail(
                subject,
                message,
                from_adress,
                recipient_list,
                fail_silently=False,
                html_message=message
            )