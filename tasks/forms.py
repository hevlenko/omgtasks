from django import forms
from django_select2.forms import Select2MultipleWidget
from django.contrib.auth.models import User
from .models import Task, Comment

from django.contrib.auth.forms import UserCreationForm


class TaskForm(forms.ModelForm):
    responsive_users = forms.ModelMultipleChoiceField(
        queryset=User.objects.all(),
        widget=Select2MultipleWidget    
    )

    class Meta:
        model = Task
        fields = ('title', 'body', 'status', 'responsive_users')  

class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ('body',)           

class UserRegisterForm(UserCreationForm):
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']
    def clean_email(self):
        data = self.cleaned_data['email']
        if User.objects.filter(email=data).exists():
            raise forms.ValidationError("This email already used")
        return data


class UserUpdateForm(forms.ModelForm):
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ['username', 'email', 'first_name', 'last_name', 'is_active']

    def clean_email(self):
        data = self.cleaned_data['email']
        if User.objects.filter(email=data).exclude(id=self.instance.id).exists():
            raise forms.ValidationError("This email already used")
        return data



class PasswordForgotForm(forms.Form):
    tmp = forms.CharField(label='Enter your username or e-mail for reset password:')
