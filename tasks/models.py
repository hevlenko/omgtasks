from django.db import models
from django import forms
from django.contrib.auth.models import User
# from django.contrib.auth.forms import UserCreationForm



# Create your models here.

class Status(models.Model):
    title = models.CharField(max_length=255)
    position = models.PositiveIntegerField()

    class Meta:
        verbose_name_plural = 'Statuses'
        ordering = ['position', '-id']

    def __str__(self):
        return self.title

class Task(models.Model):
    title = models.CharField(max_length=255)
    body = models.TextField()
    status = models.ForeignKey(
        Status, 
        blank=True, null=True, 
        on_delete=models.CASCADE,
    )

    author = models.ForeignKey(
        'auth.User', 
        on_delete=models.CASCADE)
    responsive_users = models.ManyToManyField(
        'auth.User', related_name='responsive_for',
        blank=True,
    )

    added = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-id']

    def __str__(self):
        return self.title
    
    @property
    def body_as_html(self):
        import markdown
        return markdown.markdown(self.body)

class Comment(models.Model):
    body = models.TextField()
    task = models.ForeignKey(
        Task, related_name='comments',
        on_delete=models.CASCADE)
    author = models.ForeignKey(
        'auth.User',
        on_delete=models.CASCADE)

    added = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['id']

    def __str__(self):
        return self.body[:30]

    @property
    def body_as_html(self):
        import markdown
        return markdown.markdown(self.body)

