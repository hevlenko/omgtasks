from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse, HttpResponseForbidden, Http404
from django.urls import reverse
from django.contrib.auth.decorators import login_required

from django.core.paginator import Paginator
from .models import Task, Status
from .forms import TaskForm, CommentForm, UserRegisterForm, UserUpdateForm

from django.views.generic.base import TemplateView
from django.shortcuts import render, redirect
from django.contrib import messages

from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm

from .forms import PasswordForgotForm
from django.contrib.auth.models import User
from django.core.mail import send_mail
# Create your views here.

def index(request):
    tasks = Task.objects.all()
    statuses = Status.objects.all()

    f_search = request.GET.get('search', '')
    f_status = request.GET.get('status')
    f_my = request.GET.get('my')


    selected_status = None

    if f_search:
        tasks = tasks.filter(title__icontains=f_search)
    if f_status and f_status != 'all':
        selected_status = int(f_status)
        tasks = tasks.filter(status_id=f_status)
    if f_my == 'on':
        tasks = tasks.filter(author=request.user)

    paginator = Paginator(tasks, 5, 1)
    page = request.GET.get('page')
    tasks = paginator.get_page(page)

    context = {
        'tasks': tasks,
        'statuses': statuses,
        'f_search': f_search,
        'f_status': f_status,
        'f_my': f_my,
        'selected_status': selected_status,
        }

    return render(request, 'index.html', context)

def task_view(request, id):
    task = get_object_or_404(Task, id=id)
    
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.author = request.user
            comment.task = task
            comment.save()
            return redirect(reverse('task', args=[task.id]))
    else: 
        form = CommentForm()
    return render(request, 'task.html', {
        'task': task, 'comment_form': form})

def new_task(request):
    if request.method == 'POST':
        form = TaskForm(request.POST)
        if form.is_valid:
            task = form.save(commit=False)
            task.author = request.user
            task.save()
            return redirect('/')

    else: 
        form = TaskForm()

    return render(request, 'new_task.html', {'form': form})


def edit_task(request, id):
    task = get_object_or_404(Task, id=id)

    if request.user != task.author:
        return HttpResponseForbidden('Access denied')

    if request.method == 'POST':
        form = TaskForm(request.POST, instance=task)
        if form.is_valid:
            form.save()
            return redirect(reverse('task', args=[task.id]))

    else: 
        form = TaskForm(instance=task)

    return render(request, 'edit_task.html', {'form': form})

def delete_task(request, id):
    task = get_object_or_404(Task, id=id)

    if request.user != task.author:
        return HttpResponseForbidden('Access denied')

    if request.method == 'POST':
        task.delete()
        messages.success(
            request, f'Task "{task.title}" was successfully deleted')
        return redirect('/')
    else:
        return render(request, 'delete_task.html', {'task': task})

def register(request):
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(
                request, f'Account created for {username}. You can login now')
            return redirect('/login')
    else:
        if request.user.is_authenticated:
            messages.warning(request, 'You already logged in')
            return redirect('/')
        form = UserRegisterForm()

    return render(request, 'registration/register.html', {'form': form})

def profile(request):
    return render(request, 'registration/profile.html')

def profile_change(request):
    if request.method == 'POST':
        form = UserUpdateForm(request.POST, instance=request.user)
        if form.is_valid():
            form.save()
            messages.success(
                request, f'Your account has been updated')
            return redirect('/profile')
    else:
        form = UserUpdateForm(instance=request.user)

    return render(request, 'registration/profile_change.html', {'form': form})

def password_change(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, 'Your password was successfully updated!')
            return redirect('/login')
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'registration/password_change.html', {'form': form})


def password_forgot(request):

    form = PasswordForgotForm(request.POST or None)
    users = User.objects.all()
    if request.method == 'POST':
        form = PasswordForgotForm(request.POST)
        if form.is_valid():
            user = None
            if User.objects.filter(username=form.cleaned_data['tmp']).exists():
                user = User.objects.filter(username=form.cleaned_data['tmp'])[0]
            elif User.objects.filter(email=form.cleaned_data['tmp']).exists():
                user = User.objects.filter(email=form.cleaned_data['tmp'])[0]
            if user != None and user.email:
                password = User.objects.make_random_password()
                user.set_password(password)
                user.save()
                # update_session_auth_hash(request, user)  # Important!
                subject = 'OMGTasks newsletter'
                message = '<h1>New password:</h1>'
                from_adress = 'omgtask.space'
                recipient_list = [user.email]
                message += '<p>' + str(password) + '</p>'
                send_mail(
                    subject,
                    message,
                    from_adress,
                    recipient_list,
                    fail_silently=False,
                    html_message=message
                )
                messages.success(
                    request, f'Your send new password to e-mail')
                return redirect('/login')
        messages.success(request, f'Repeat... Unknown user or e-mail (empty e-mail)')
    return render(request, 'registration/password_forgot.html', {'form': form})





